from django.apps import AppConfig


class NotekeeperRegisterConfig(AppConfig):
    name = 'noteKeeper_register'
